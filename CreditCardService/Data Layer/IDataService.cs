﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CreditCardService.Data_Layer
{
    interface IDataService<T>
    {
        Task Register(T newT, string tableName);
        Task<List<T>> GetAllAsync(string tableName);
        Task<T> GetAsync(string tableName, string queryInput, string value);
        void UpsertAsync(string tableName, string queryInput, string value, T record);
        Task DeleteAsync(string tableName, string queryInput, string value);

    }
}
