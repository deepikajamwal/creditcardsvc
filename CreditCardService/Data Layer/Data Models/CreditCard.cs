﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace CreditCardService.Data_Layer.Data_Models
{
    public class CreditCard
    {
        [BsonId]
        public Guid Id { get; set; }
        
        [BsonElement("CardNumber")]
        public string CardNumber { get; set; }

        [BsonElement("DebitedAmount")]
        public double DebitedAmount { get; set; }

        [BsonElement("CreditedAmount")]
        public double CreditedAmount { get; set; }

        [BsonElement("Balance")]
        public double Balance { get; set; }

        [BsonElement("ExpiryDate")]
        public DateTime Expiry { get; set; }
        [BsonElement("Cvv")]
        public string CVV { get; set; }

        [BsonElement("NameOnCard")]
        public String NameOnCard { get; set; }


       
    }

 
}
