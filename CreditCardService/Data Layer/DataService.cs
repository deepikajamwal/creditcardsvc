﻿using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CreditCardService.Data_Layer
{
    public class DataService<T>:IDataService<T>
    {
        private List<T> list = new List<T>();
        public const string connectionString = "mongodb://localhost:27017";
        IMongoDatabase _db;
        MongoClient _client;

        public DataService(string databaseName)
        {
            _client = new MongoClient(connectionString);
            _db = _client.GetDatabase(databaseName);
        }

        public async Task Register(T newT, string tableName)
        {
            await _db.GetCollection<T>(tableName).InsertOneAsync(newT);
        }

        public async Task<List<T>> GetAllAsync(string tableName)
        {
            return await _db.GetCollection<T>(tableName).Find<T>(new BsonDocument()).ToListAsync();
        }

        public async Task<T> GetAsync(string tableName, string queryInput, string value)
        {
            return await _db.GetCollection<T>(tableName).Find<T>(Builders<T>.Filter.Eq(queryInput, value)).FirstOrDefaultAsync();
        }

        public async void UpsertAsync(string tableName, string queryInput, string value, T record)
        {
            await _db.GetCollection<T>(tableName).ReplaceOneAsync(new BsonDocument(queryInput, value), record, new ReplaceOptions { IsUpsert = true });
        }

        public async Task DeleteAsync(string tableName, string queryInput, string value)
        {
           var res =  await _db.GetCollection<T>(tableName).DeleteOneAsync(Builders<T>.Filter.Eq(queryInput, value));
            if(res.DeletedCount == 0)
            {
                throw new Exception("Record does not exist");
            }
        }
    }
}
