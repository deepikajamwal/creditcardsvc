﻿using CreditCardService.Business_Layer.Validation;
using CreditCardService.Data_Layer;
using CreditCardService.Data_Layer.Data_Models;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CreditCardService.Business_Layer
{
    public class CreditCardBusinessService
    {
        private DataService<CreditCard> _creditCardDataService;
        private const string creditCardsTableName = "CreditCards";
        public CreditCardBusinessService()
        {
            _creditCardDataService = new DataService<CreditCard>("CreditCardsDb");
        }

        public async Task Register(CreditCard newCard)
        {
            try
            {
                //Validate
                var validationResult = CreditCardValidationservice.ValidateNew(newCard);
                if (!validationResult.HasPassed)
                    throw new Exception(validationResult.Errors);
                else
                {
                    //Check if card exists
                    var existingCard =await _creditCardDataService.GetAsync(creditCardsTableName, "CardNumber", newCard.CardNumber);
                    if (existingCard != null)
                        throw new Exception("This card is already registered");
                    else await _creditCardDataService.Register(newCard, creditCardsTableName);
                   
                }
            }
            catch
            { throw; }
        }

        public async Task<List<CreditCard>> GetAllAsync()
        {
            return await _creditCardDataService.GetAllAsync(creditCardsTableName);
        }

        public async Task<CreditCard> GetAsync(string cardNumber)
        {
            //Validate
            var validationResult = CreditCardValidationservice.Validate(cardNumber);
            if (!validationResult.HasPassed)
                throw new Exception(validationResult.Errors);
            else
            {
                var record = await _creditCardDataService.GetAsync(creditCardsTableName, "CardNumber", cardNumber);
                if (record == null)
                    throw new Exception("Card not registered");
                else return record;
            }
        }

        public async Task UpdateBalanceAsync(CreditCard existingCard)
        {
            //Validate
            var validationResult = CreditCardValidationservice.ValidateBalanceUpdate(existingCard);
            if (!validationResult.HasPassed)
                throw new Exception(validationResult.Errors);
            else
            {
                //Check if Card exists
                var dbCard = await _creditCardDataService.GetAsync(creditCardsTableName, "CardNumber", existingCard.CardNumber);
                if (dbCard == null)
                    throw new Exception("This Card is not registered");
                else
                {
                    if (existingCard.DebitedAmount > 0)
                        dbCard.Balance = dbCard.Balance - existingCard.DebitedAmount;
                    else if (existingCard.CreditedAmount > 0)
                        dbCard.Balance = dbCard.Balance + existingCard.CreditedAmount;
                    _creditCardDataService.UpsertAsync(creditCardsTableName, "CardNumber", dbCard.CardNumber, dbCard);
                }
            }
        }
        public async Task DeleteAsync(string cardnumber)
        {
            //Validate
            var validationResult = CreditCardValidationservice.Validate(cardnumber);
            if (!validationResult.HasPassed)
                throw new Exception(validationResult.Errors);
            else
                await _creditCardDataService.DeleteAsync(creditCardsTableName, "CardNumber", cardnumber);
        }
    }
}
