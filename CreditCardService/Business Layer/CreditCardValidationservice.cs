﻿using CreditCardService.Business_Layer.Validation;
using CreditCardService.Data_Layer.Data_Models;
using System;
using System.Linq;

namespace CreditCardService.Business_Layer
{
	public static class CreditCardValidationservice
    {
		private static bool Luhn(string cardNumber)
		{
			return cardNumber.Reverse()
				.Select(c => c - 48) // to convert to integer
				.Select((num, i) => i % 2 == 0	? num : ((num *= 2) > 9 ? num - 9 : num)
				).Sum() % 10 == 0;
		}
		private static bool StringNotEmpty(string s) => !string.IsNullOrEmpty(s);
		private static ValidationResult CreditCardDigitsValidationRule(string cardNumber)
		{
			return new Validator<string>(cardNumber, "Card Number")
						.AddRule((c) => !string.IsNullOrEmpty(c) &&
										c.All(char.IsDigit) &&
										c.Length == 16,
										"should be 16 digits")
						.AddRule(Luhn, "should be Valid")
						.GetResult();
		}
		private static ValidationResult CreditCardExpiryDateValidationRule(DateTime expiry)
		{
			return new Validator<DateTime>(expiry, "Expiry")
						.AddRule((e) => (DateTime.Now < e), "This Card has expired")
						.GetResult();
		}
		private static ValidationResult CreditCardNameValidationRule(string name)
		{
			return new Validator<string>(name, "Name on Card")
						.AddRule(StringNotEmpty, "is required")
						.GetResult();
		}
		private static ValidationResult CreditCardCvvValidationRule(string cvv)
		{
			return new Validator<string>(cvv, "CVV")
						.AddRule((c) => !string.IsNullOrEmpty(c) &&
										c.All(char.IsDigit) &&
										c.Length == 3, "should be 3 digits")
						.GetResult();
		}
		private static ValidationResult CreditCardDebitedAmtValidationRule(double debit, double balance)
		{
			return new Validator<double>(debit, "Debited Amount")
						.AddRule((c) => c <= balance, "should be less than Balance")
						.GetResult();
		}

		private static ValidationResult CreditCardDebitCreditInputValidationRule(double debit, double credit)
		{
			return new Validator<double>(debit, credit, "Debited Amount / Credited Amount", "Debited Amount /Credited Amount")
						.AddRule((d, c) => d != 0 || c != 0, "Either should be entered")
						.GetResult();
		}
		public static CreditCardValidationResult ValidateNew(CreditCard card)
        {
			var results = new[]
				{
					CreditCardDigitsValidationRule(card.CardNumber),
					CreditCardExpiryDateValidationRule(card.Expiry),
					CreditCardNameValidationRule(card.NameOnCard),
					CreditCardCvvValidationRule(card.CVV),
					CreditCardDebitedAmtValidationRule(card.DebitedAmount,card.Balance)
				}
			.Where(r => r.HasErrors)
			.ToArray();

			CreditCardValidationResult result;
			if (results.Any())
			{
				result = new CreditCardValidationResult(false, string.Join(Environment.NewLine, results.Select(r => r.Message())));
			}
			else result = new CreditCardValidationResult(true);

			return result;
		}

		public static CreditCardValidationResult ValidateBalanceUpdate(CreditCard card)
		{
			var results = new[]
				{
					CreditCardDigitsValidationRule(card.CardNumber),
					CreditCardDebitCreditInputValidationRule(card.DebitedAmount,card.CreditedAmount),
					CreditCardDebitedAmtValidationRule(card.DebitedAmount,card.Balance),
				}
			.Where(r => r.HasErrors)
			.ToArray();

			CreditCardValidationResult result;
			if (results.Any())
			{
				result = new CreditCardValidationResult(false, string.Join(Environment.NewLine, results.Select(r => r.Message())));
			}
			else result = new CreditCardValidationResult(true);

			return result;
		}

		public static CreditCardValidationResult Validate(string cardNumber)
		{
			var results = new[]
				{
					CreditCardDigitsValidationRule(cardNumber)
				}
			.Where(r => r.HasErrors)
			.ToArray();

			CreditCardValidationResult result;
			if (results.Any())
			{
				result = new CreditCardValidationResult(false, string.Join(Environment.NewLine, results.Select(r => r.Message())));
			}
			else result = new CreditCardValidationResult(true);

			return result;
		}

	}
}
