﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CreditCardService.Business_Layer.Validation
{
    public class CreditCardValidationResult
    {
        public bool HasPassed { get; set; }
        public string Errors { get; set; }

        public CreditCardValidationResult(bool success, string errors=null)
        {
            HasPassed = success;
            Errors = errors;
        }
    }
}
