﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CreditCardService.Business_Layer.Validation
{
	public class Validator<T>
	{
		private readonly T _value;
		private readonly string _fieldName;
		private readonly T _value2;
		private readonly string _fieldName2;
		private readonly HashSet<string> _validationErrors;
		public Validator(T value, string fieldName)
		{
			_value = value;
			_fieldName = fieldName;
			_validationErrors = new HashSet<string>();
		}

		public Validator(T value,T value2, string fieldName,string fieldName2)
		{
			_value = value;
			_fieldName = fieldName;
			_value2 = value2;
			_fieldName2 = fieldName2;
			_validationErrors = new HashSet<string>();
		}

		public ValidationResult GetResult() =>
			new ValidationResult(_fieldName, _validationErrors, HasErrors);

		public ValidationResult GetResult2() =>
			new ValidationResult(_fieldName,_fieldName2, _validationErrors, HasErrors);
		private bool HasErrors => _validationErrors.Any();

		public Validator<T> AddRule(Func<T, bool> rule, string message)
		{
			if (!rule(_value))
				_validationErrors.Add(message);
			return this;
		}
		public Validator<T> AddRule(Func<T,T, bool> rule, string message)
		{
			if (!rule(_value,_value2))
				_validationErrors.Add(message);
			return this;
		}
	}

	public class ValidationResult
	{
		public string FieldName { get; }
		public string FieldName2 { get; }
		public ICollection<string> ValidationErrors { get; }
		public bool HasErrors { get; }
		public ValidationResult(string fieldName, ICollection<string> validationErrors, bool hasErrors)
		{
			FieldName = fieldName;
			ValidationErrors = validationErrors;
			HasErrors = hasErrors;
		}
		public ValidationResult(string fieldName,string fieldName2, ICollection<string> validationErrors, bool hasErrors)
		{
			FieldName = fieldName;
			FieldName2 = fieldName2;
			ValidationErrors = validationErrors;
			HasErrors = hasErrors;
		}

		public string Message() => string.Join(", ", ValidationErrors.Select(error => $"{FieldName}: {error}"));
	}
}