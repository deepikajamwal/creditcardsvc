﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CreditCardService.Business_Layer;
using CreditCardService.Data_Layer.Data_Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CreditCardService.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CreditCardsController : ControllerBase
    {
        private CreditCardBusinessService _service;
        public CreditCardsController()
        {
            _service = new CreditCardBusinessService();
        }
        // GET: api/CreditCards
        [HttpGet]
        public async Task<List<CreditCard>> Get()
        {
            return await _service.GetAllAsync();
        }

        // GET: api/CreditCards/<name>
        [HttpGet("{cardnumber}", Name = "Get")]
        public async Task<IActionResult> Get(string cardnumber)
        {
            try
            {
                return Ok( await _service.GetAsync(cardnumber));
                
            }
            catch (Exception ex)
            {
               return BadRequest(ex.Message);
            }
        }

        // POST: api/CreditCards
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] Data_Layer.Data_Models.CreditCard input)
        {
           
            try
            {
                await _service.Register(input);
                return Ok("Credit Card registered");

            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        // PUT: api/CreditCards/5
        [HttpPut]
        public async Task<IActionResult> Put([FromBody] Data_Layer.Data_Models.CreditCard input)
        {
            try
            {
                await _service.UpdateBalanceAsync(input);
                return Ok("Balance updated");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{cardnumber}")]
        public async Task<IActionResult> Delete(string cardnumber)
        {
            try
            {
                await _service.DeleteAsync(cardnumber);
                return Ok("Card deleted");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
