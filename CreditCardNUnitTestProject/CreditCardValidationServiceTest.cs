using CreditCardService.Business_Layer;
using CreditCardService.Data_Layer.Data_Models;
using NUnit.Framework;
using System;

namespace CreditCardNUnitTestProject
{
    [TestFixture]
    public class CreditCardValidationServiceTest
    {
        CreditCard newCard = null;
        [SetUp]
        public void Setup()
        {
            newCard = new CreditCard();
            newCard.NameOnCard = "DJ";
            newCard.CardNumber = "5497772214011713";
            newCard.Balance = 1000;
            newCard.CVV = "083";
            newCard.Expiry = DateTime.Now.AddDays(2);
        }

        [Test]
        public void CreditCardValidationServiceTest_ValidCard()
        {
            Assert.IsTrue(CreditCardValidationservice.ValidateNew(newCard).HasPassed);
        }

        [Test]
        [TestCase("549777221401171", "Card Number: should be 16 digits")]
        [TestCase("", "Card Number: should be 16 digits")]
        [TestCase("5497772214011711", "Card Number: should be Valid")]
        public void CreditCardValidationServiceTest_CardNumber_InvalidEntries(string cardnumber, string expectedResult)
        {
            newCard.CardNumber = cardnumber;
            var result = CreditCardValidationservice.ValidateNew(newCard);
            Assert.False(result.HasPassed);
            Assert.IsTrue(result.Errors.Contains(expectedResult));
        }

        [Test]
        [TestCase("549777221401171", "CVV: should be 3 digits")]
        [TestCase("", "CVV: should be 3 digits")]
        public void CreditCardValidationServiceTest_Cvv_InvalidEntries(string cvv, string expectedResult)
        {
            newCard.CVV = cvv;
            var result = CreditCardValidationservice.ValidateNew(newCard);
            Assert.False(result.HasPassed);
            Assert.IsTrue(result.Errors.Contains(expectedResult));
        }

        [Test]
        [TestCase("Deepika", "2007-01-01", "Expiry: This Card has expired")]
        [TestCase("", "2020-2-2", "Name on Card: is required")]
        public void CreditCardValidationServiceTest_Card_InvalidEntries(string name, string expirydate, string expectedResult)
        {
            newCard.NameOnCard = name;
            newCard.Expiry = DateTime.Parse(expirydate);
            var result = CreditCardValidationservice.ValidateNew(newCard);
            Assert.False(result.HasPassed);
            Assert.IsTrue(result.Errors.Contains(expectedResult));
        }
    }
}

